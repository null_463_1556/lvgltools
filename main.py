# This Python file uses the following encoding: utf-8
import shutil
import sys
import os
import time
import logging
import subprocess

from urllib3 import encode_multipart_formdata
import requests
from PIL import Image
from PySide2.QtWidgets import *
from PySide2.QtCore import *
from PySide2.QtUiTools import *
from PySide2.QtGui import *


class ImageConvert(QWidget):
    def __init__(self, parent=None):
        super(ImageConvert, self).__init__(parent)
        self.load_ui()
        self.init_ui()
        self.init_listwidget()
        self.init_color_format_combobox()
        self.init_format_combobox()
        self.imageconvert_thread = ImageConvertThread(self)
        self.imageconvert_thread.sin_out.connect(self.signal_imge_convert_finish)
        #self.test()

    def load_ui(self):
        loader = QUiLoader()
        path = os.path.join(os.getcwd(), "form.ui")
        ui_file = QFile(path)
        ui_file.open(QFile.ReadOnly)
        self.ui = loader.load(ui_file, self)
        self.setWindowTitle('ASR-LVGLTools V2.0')
        self.setWindowIcon(QIcon('icon.ico'))
        ui_file.close()

    def init_ui(self):
        self._init_treewidget()
        self._init_button()
        self.init_ttf_param()
        self.init_lvgl7_ui()

    def init_ttf_param(self):
        self.ui.height_lineEdit.setText('20')
        self.ui.bpp_lineEdit.setText('2')
        self.ui.uni_first_lineEdit.setText('127')
        self.ui.uni_last_lineEdit.setText('1114111')

    def _init_treewidget(self):
        self.ui.ttfTreeWidget.setHeaderLabels(['文件名', '状态'])
        self.ui.ttfTreeWidget.setColumnWidth(0, 650)
        self.ui.ttfTreeWidget.setColumnWidth(1, 100)
        self.ui.ttfTreeWidget.setIconSize(QSize(20, 20))
        #        self.ui.ttfTreeWidget.doubleClicked.connect(self.onTwContentClicked)
        self.ui.ttfTreeWidget.setRootIsDecorated(False)

    def _init_button(self):
        self.ui.pb_select_files.clicked.connect(self.slot_pb_select_files_click)
        self.ui.pb_convert.clicked.connect(self.slot_pb_convert_click)
        self.ui.pb_ttf_file_choice.clicked.connect(self.slot_pb_ttf_file_choice_click)
        self.ui.pb_ttf_convert.clicked.connect(self.slot_pb_ttf_convert_click)

    def init_listwidget(self):
        self.model = QStandardItemModel(0, 2)
        self.model.setHorizontalHeaderLabels(['文件', '状态']);
        self.ui.tableView.setModel(self.model)
        self.ui.tableView.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.ui.tableView.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.ui.tableView.horizontalHeader().setSectionResizeMode(0, QHeaderView.Stretch)


    def init_color_format_combobox(self):
        self.color_format_dict = {'True color': 'true_color',
                                  'True color with alpha': 'true_color_alpha',
                                  'True color chroma keyed': 'true_color_chroma',
                                  'Indexed 2 colors': 'indexed_1',
                                  'Indexed 4 colors': 'indexed_2',
                                  'Indexed 16 colors': 'indexed_4',
                                  'Indexed 256 colors': 'indexed_8',
                                  'Alpha only 2 shades': 'alpha_1',
                                  'Alpha only 4 shades': 'alpha_2',
                                  'Alpha only 16 shades': 'alpha_4',
                                  'Alpha only 256 shades': 'alpha_8',
                                  'Raw': 'raw',
                                  'Raw with alpha': 'raw_alpha',
                                  'Raw as chroma keyed': 'raw_chroma'}
        self.ui.cbb_color_format.addItems(self.color_format_dict.keys())

    def init_format_combobox(self):
        self.format_dict = {'C array': 'c_array',
                            'Binary RGB332': 'bin_332',
                            'Binary RGB565': 'bin_565',
                            'Binary RGB565 Swap': 'bin_565_swap',
                            'Binary RGB888': 'bin_888', }
        self.ui.cbb_format.addItems(self.format_dict.keys())

        self.ui.lvgl_Combox.addItems(['lvgl5', 'lvgl7'])
        self.ui.lvgl_Combox.currentIndexChanged.connect(self.slot_lvgl_combox_index_change)
        self.ui.lvgl7_frame.hide()

    def init_lvgl7_ui(self):
        self.ui.lvgl7_size_lineEdit.setText('20')
        self.ui.lvgl7_bpp_comboBox.addItems(['1', '2', '3', '4', '8'])
        self.ui.lvgl7_bpp_comboBox.setCurrentIndex(1)
        self.ui.lvgl7_range_lineEdit.setText('0x20-0x7F,0xB0,0x2022')
        self.ui.lvgl7_compressed_comboBox.addItems(['False', 'True'])
        self.ui.lvgl7_format_comboBox.addItems(['lvgl', 'bin'])

    @Slot()
    def slot_pb_select_files_click(self):
        files, filetype = QFileDialog.getOpenFileNames(self,
                                                       "多文件选择",
                                                       os.getcwd(),  # 起始路径
                                                       "Image Files (*.png;*.gif;*jpg;*jpeg);;Audio Files(*.mid;*.amr);;TTF Fils(*.ttf)")

        if len(files) > 0:
            self.init_listwidget()
            for i in range(0, len(files)):
                model_item = QStandardItem(files[i])
                self.model.setItem(i, 0, model_item)
                model_item = QStandardItem('未转换')
                self.model.setItem(i, 1, model_item)

    @Slot()
    def slot_pb_convert_click(self):
        if self.ui.pb_convert.text() == '开始转换':
            self.imageconvert_thread.start()
            self.ui.pb_convert.setText('停止转换')
            self.ui.pb_select_files.setEnabled(False)
        else:
            self.imageconvert_thread.stop()
            self.ui.pb_convert.setText('开始转换')
            self.ui.pb_select_files.setEnabled(True)

    def signal_imge_convert_finish(self, msg):
        QMessageBox.information(self, '提示', msg, QMessageBox.Yes, QMessageBox.Yes)

    @Slot()
    def slot_pb_ttf_file_choice_click(self):
        files, filetype = QFileDialog.getOpenFileNames(self,
                                                       "多文件选择",
                                                       os.getcwd(),  # 起始路径
                                                       "TTF Fils(*.ttf)")

        if len(files) > 0:
            self.ui.ttfTreeWidget.clear()
            for i in range(0, len(files)):
                twItem = QTreeWidgetItem(self.ui.ttfTreeWidget)
                twItem.setText(0, files[i])
                twItem.setText(1, '未转换')

    @Slot()
    def slot_pb_ttf_convert_click(self):
        if self.ui.pb_ttf_convert.text() == '开始转换':
            self.fct = FontConvertThread(self)
            self.fct.sin_out.connect(self.signal_imge_convert_finish)
            self.fct.start()
            self.ui.pb_ttf_convert.setText('停止转换')
            self.ui.pb_ttf_file_choice.setEnabled(False)
        else:
            self.fct.stop()
            self.ui.pb_ttf_convert.setText('开始转换')
            self.ui.pb_ttf_file_choice.setEnabled(True)


    def slot_lvgl_combox_index_change(self):
        if self.ui.lvgl_Combox.currentIndex() == 0:
            self.ui.lvgl5_frame.show()
            self.ui.lvgl7_frame.hide()
        else:
            self.ui.lvgl5_frame.hide()
            self.ui.lvgl7_frame.show()



class FontConvertThread(QThread):
    sin_out = Signal(str)

    def __init__(self, parent=None):
        self.win = parent
        self.text_file = 'chinese.txt'
        super(FontConvertThread, self).__init__(parent)

    def stop(self):
        if self.b_running:
            self.win.ui.pb_ttf_convert.setEnabled(False)
        self.b_running = False
        print('FontConvertThread stop')

    def start(self):
        print('FontConvertThread start')
        self.b_running = True
        super().start()

    def run(self):
        item = QTreeWidgetItemIterator(self.win.ui.ttfTreeWidget)
        if item.value() == None:
            info = '请选择文件'
        else:
            info = '转换完成'
        while item.value():
            print(item.value().text(0))
            item.value().setText(1, '正在转换')
            if (self.win.ui.lvgl_Combox.currentIndex == 0):
                ret = self.convert_offline(item.value().text(0), self.win.ui.height_lineEdit.text(), self.win.ui.bpp_lineEdit.text(),
                                           self.win.ui.uni_first_lineEdit.text(), self.win.ui.uni_last_lineEdit.text())
            else:
                ret = self.convert_offline_lvgl7(item.value().text(0),
                                                 self.win.ui.lvgl7_size_lineEdit.text(),
                                                 self.win.ui.lvgl7_bpp_comboBox.currentText(),
                                                 self.win.ui.lvgl7_range_lineEdit.text(),
                                                 self.win.ui.lvgl7_symbols_lineEdit.text(),
                                                 self.win.ui.lvgl7_compressed_comboBox.currentText(),
                                                 self.win.ui.lvgl7_format_comboBox.currentText(),)
            if ret == False:
                item.value().setText(1, '转换失败')
            else:
                item.value().setText(1, '转换完成')

            if self.b_running == False:
                break;

            item = item.__iadd__(1)
        self.win.ui.pb_ttf_file_choice.setEnabled(True)
        self.win.ui.pb_ttf_convert.setText('开始转换')
        self.win.ui.pb_ttf_convert.setEnabled(True)
        print('ImageConvertThread exit')
        self.sin_out.emit(info)

    def convert_offline_lvgl7(self, ttf_name, size, bpp, range, symbols, compressd, format):
        lvgl7_path = os.path.join(os.getcwd(), 'lvgl7_font')
        file = os.path.join(lvgl7_path, 'Chinese.txt')
        lv_font_conv = os.path.join(lvgl7_path, 'lv_font_conv.exe')
        ttf_base_name = os.path.basename(ttf_name)
        default_ttf = os.path.join(lvgl7_path, 'Montserrat-Medium.ttf')
        default_woff = os.path.join(lvgl7_path, 'FontAwesome5-Solid+Brands+Regular.woff')
        if format == 'lvgl':
            if compressd == 'False':
                output = os.path.join(os.path.dirname(ttf_name), ttf_base_name.split('.')[0] + '.c')
            else:
                output = os.path.join(os.path.dirname(ttf_name), ttf_base_name.split('.')[0] + '_compressed.c')
        else:
            if compressd == 'False':
                output = os.path.join(os.path.dirname(ttf_name), ttf_base_name.split('.')[0] + '.bin')
            else:
                output = os.path.join(os.path.dirname(ttf_name), ttf_base_name.split('.')[0] + '_compressed.bin')
        if compressd == 'False':
            compr = "--no-compress --no-prefilter"
        else:
            compr = ""
        if len(symbols.strip()) > 0:
            symbols = "--symbols " + symbols

        syms = "61441,61448,61451,61452,61452,61453,61457,61459,61461,61465,61468,61473,61478,61479,61480,61502,61512,61515,61516,61517,61521,61522,61523,61524,61543,61544,61550,61552,61553,61556,61559,61560,61561,61563,61587,61589,61636,61637,61639,61671,61674,61683,61724,61732,61787,61931,62016,62017,62018,62019,62020,62087,62099,62212,62189,62761,62764,62810,63426,63650"
        cmd = "{} {} --bpp {} --size {} --font {} -r {} {} --font {} -r {} --font {} --file {} --format {} -o {} --force-fast-kern-format".format(
            lv_font_conv, compr, bpp, size, default_ttf, range, symbols, default_woff, syms, ttf_name, file, format, output)
        print(cmd)
        p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
        error = p.stdout.readlines()
        print('zmxin: ' + str(error) + ' len = ' + str(len(error)))



    def convert_offline(self, ttf_name, height, bpp, uni_first, uni_last):
        ttf_base_name = os.path.basename(ttf_name).split('.')[0]
        tools_path = os.path.join(os.getcwd(), 'tools')
        php_path = os.path.join(tools_path, 'php.exe')
        self.text_file = os.path.join(tools_path, self.text_file)
        tff_php_path = os.path.join(tools_path, 'font_conv_core.php')
        cmd = '"name=' + ttf_base_name + '&font=' + ttf_name + '&height=' + height + '&bpp=' + bpp \
                + '&text_file=' + self.text_file + '&uni_first=' + uni_first + '&uni_last=' + uni_last \
                + '&built_in=0"'
        cmd = php_path + ' ' + tff_php_path + ' ' + cmd
        print('cmd = ' + cmd)
        p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
        error = p.stdout.readlines()
        log_path = os.path.join(os.getcwd(), 'logs')
        if os.path.exists(log_path) == False:
            os.mkdir(log_path)
        log_file = open(os.path.join(log_path, os.path.basename(ttf_name) + '.log'), 'wb')
        log_file.writelines(error)
        log_file.flush()
        log_file.close()
        c_file = os.path.join(os.getcwd(), ttf_base_name + '.c')
        if os.path.exists(c_file) == True:
            dst_path = os.path.join(os.path.dirname(ttf_name), ttf_base_name + '.c')
            cur_path = os.path.join(os.getcwd(), ttf_base_name + '.c')
            if os.path.dirname(ttf_name) != os.getcwd().replace('\\', '/'):
                if os.path.exists(dst_path) == True:
                    os.remove(dst_path)
                try:
                    shutil.move(cur_path, dst_path)
                except:
                    print('ttf_raw_path = ' + os.path.dirname(ttf_name))
                    print('cwdpath = ' + os.getcwd().replace('\\', '/'))
            return True
        else:
            return False


class ImageConvertThread(QThread):
    sin_out = Signal(str)

    def __init__(self, parent=None):
        self.win = parent
        self.b_running = True;
        self.log_file = 'log.txt'
        super(ImageConvertThread, self).__init__(parent)

    def stop(self):
        if self.b_running:
            self.win.ui.pb_convert.setEnabled(False)
        self.b_running = False
        print('thread stop')

    def start(self):
        print('start')
        self.b_running = True
        super().start()

    def run(self):
        print('thread run')
        print(self.win.model.rowCount())
        if self.win.model.rowCount() <= 0:
            info = '请先选择文件'
        else:
            info = '转换完成'
        for i in range(0, self.win.model.rowCount()):
            print(self.win.model.item(i).text())
            self.win.model.item(i, 1).setText('正在转换')
            '''
            convert_ret = self.convert(self.win.model.item(i).text(), self.win.format_dict.get(self.win.ui.cbb_format.currentText()),
                         self.win.color_format_dict.get(self.win.ui.cbb_color_format.currentText()))
           '''
            convert_ret = self.convert_offline(self.win.model.item(i).text(),
                                               self.win.format_dict.get(self.win.ui.cbb_format.currentText()),
                                               self.win.color_format_dict.get(
                                                   self.win.ui.cbb_color_format.currentText()))
            if convert_ret == False:
                self.win.model.item(i, 1).setText('转换失败')
            else:
                self.win.model.item(i, 1).setText('转换完成')
            if self.b_running == False:
                break
        self.win.ui.pb_select_files.setEnabled(True)
        self.win.ui.pb_convert.setText('开始转换')
        self.win.ui.pb_convert.setEnabled(True)
        print('ImageConvertThread exit')
        self.sin_out.emit(info)

    def convert_offline(self, path, format, cf):
        pic_suffixs = ['jpg', 'jpeg', 'png']
        is_pic = False
        dst_path = path
        for suffix in pic_suffixs:
            if path.endswith(suffix):
                is_pic = True
                break
        if is_pic == False:
            dst_path = path.split('.')[0] + '.png'
            os.rename(path, dst_path)

        print('dst_path = ' + dst_path + ' path = ' + path)
        php = os.path.join(os.getcwd(), 'tools', 'php.exe') + ' ' + os.path.join(os.getcwd(), 'tools', 'img_conv_core.php') + ' "name=' + \
              os.path.basename(dst_path).split('.')[0] + '&img=' + dst_path + '&format=' + format + '&cf=' + cf + '"'
        print('php = ' + php)
        p = subprocess.Popen(php, shell=True, stdout=subprocess.PIPE)
        error = p.stdout.readlines()
        if is_pic == False:
            os.rename(dst_path, path)
        if len(error) == 0:
            b_path = os.path.dirname(path) == os.getcwd().replace('\\', '/')
            gen_suffix = '.c'
            if format == 'c_array':
                gen_suffix = '.c'
            else:
                gen_suffix = '.bin'
            if b_path == False:
                path2 = os.path.join(os.path.dirname(path), os.path.basename(dst_path).split('.')[0] + gen_suffix)
                dst_file = path2
                print('path2 = ' + path2)
                if os.path.exists(path2):
                    os.remove(path2)
                cur_path = os.path.join(os.getcwd(), os.path.basename(dst_path).split('.')[0] + gen_suffix)
                print('cur_path = ' + cur_path)
                shutil.move(cur_path, os.path.dirname(path))
            return True
        else:
            log_path = os.path.join(os.getcwd(), 'logs')
            if os.path.exists(log_path) == False:
                os.mkdir(log_path)
            log_path = os.path.join(log_path, os.path.basename(dst_path) + '.log')
            f_log = open(log_path, 'wb')
            f_log.writelines(error)
            f_log.flush()
            f_log.close()
            return False


def convert(self, path, format, cf):
    base_name = os.path.basename(path)
    pre_suffix = base_name.split('.')[0]
    if path.endswith('.mid') or path.endswith('.amr'):
        pre_suffix = 'audio_' + pre_suffix
    elif path.endswith('.ttf'):
        pre_suffix = 'lv_font_' + pre_suffix + '_ttf'
    files = {'customFile': (base_name,
                            open(path, 'rb').read(),
                            'image/png',
                            )}
    data = {'name': pre_suffix, 'format': format, 'cf': cf}
    try:
        self.s = requests.session()
        # https://lvgl.io/tools/img_conv_core.php
        # https://lvgl.io/tools/imageconverter
        # r = self.s.post('https://lvgl.io/tools/img_conv_core.php', data=data, files=files, timeout = (60, 60))
        r = requests.post('https://lvgl.io/tools/img_conv_core.php', data=data, files=files, timeout=(60, 60))
        c_content = r.text;
        self.s.close()
        print(c_content)
        if format == 'c_array':
            c_content = c_content.replace('#if defined(LV_LVGL_H_INCLUDE_SIMPLE)', '') \
                .replace('#include "lvgl.h"', '') \
                .replace('#else', '') \
                .replace('#include "lvgl/lvgl.h"', '') \
                .replace('#endif', '') \
                .replace('#ifndef LV_ATTRIBUTE_MEM_ALIGN', '') \
                .replace('#define LV_ATTRIBUTE_MEM_ALIGN', '') \
                .replace('#endif', '') \
                .replace('#ifndef LV_ATTRIBUTE_IMG_' + pre_suffix.upper(), '') \
                .replace('#define LV_ATTRIBUTE_IMG_' + pre_suffix.upper(), '') \
                .replace('#endif', '') \
                .replace('LV_ATTRIBUTE_LARGE_CONST LV_ATTRIBUTE_IMG_' + pre_suffix.upper(), '')

            const_index = c_content.index('const')
            if path.endswith('.gif') or path.endswith('.mid') or path.endswith('.amr'):
                include_header = '#include "lvgl/lvgl.h"'
                if path.endswith('.mid') or path.endswith('.amr'):
                    include_header += '\n#include "lvgl/hal/hal.h"'
            elif path.endswith('.ttf'):
                include_header = '#include "lv_conf.h"'
                include_header += '\n#include "lvgl/lvgl.h"'
            else:
                include_header = '#include "lv_conf.h"\n#include "lvgl/lv_draw/lv_draw_img.h"'
            c_content = include_header + '\n\n#ifndef LV_ATTRIBUTE_MEM_ALIGN\n#define LV_ATTRIBUTE_MEM_ALIGN\n#endif\n\n\n' + c_content[
                                                                                                                              const_index:]

            if path.endswith('.gif') or path.endswith('.png') or path.endswith('.jpg') or path.endswith('.jpeg'):
                image_size = Image.open(path).size
                image_width = image_size[0]
                image_height = image_size[1]
                header_w = 'header.w = '
                header_h = 'header.h = '
                try:
                    header_w_index = c_content.index(header_w + str(image_width))
                    print('zmxing = %d' % header_w_index)
                except Exception as err:
                    print(err)
                    c_content = c_content.replace(header_w, header_w + str(image_width))
                    c_content = c_content.replace(header_h, header_h + str(image_height))
                    c_content = c_content.replace('const lv_img_dsc_t', 'lv_img_dsc_t')
            elif path.endswith('.mid') or path.endswith('.amr'):
                t_index = c_content.index('const lv_img_dsc_t')
                c_content = c_content[:t_index]
                var_define = 'lv_audio_dsc_t ' + pre_suffix + ' = {\n\t.data_size = ' + str(
                    os.path.getsize(path)) + ',\n\t'
                var_define += '.data = ' + pre_suffix + '_map,\n};'
                c_content += var_define
            elif path.endswith('.ttf'):
                t_index = c_content.index('const lv_img_dsc_t')
                c_content = c_content[:t_index]
                var_define = 'lv_font_fmt_freetype_src_t ' + pre_suffix + '_src = {\n\t.data_size = ' + str(
                    os.path.getsize(path)) + ',\n\t'
                var_define += '.data = (uint8_t *)' + pre_suffix + '_map,\n};\n\n'
                var_define += 'lv_font_t ' + pre_suffix + ' = {\n\t'
                var_define += '.unicode_first = 127,   /*First Unicode letter in this font*/\n\t'
                var_define += '.unicode_last = 1114111,    /*Last Unicode letter in this font*/\n\t'
                var_define += '.h_px = 20,             /*Font height in pixels, real width and height need read for FT face */\n\t'
                var_define += '.glyph_bitmap = NULL,  /*Bitmap of glyphs*/\n\t'
                var_define += '.glyph_dsc = NULL,        /*Description of glyphs*/\n\t'
                var_define += '.glyph_cnt = 6507,          /*Number of glyphs in the font*/\n\t'
                var_define += '.unicode_list = NULL,  /*List of unicode characters*/\n\t'
                var_define += '.get_bitmap = lv_font_get_bitmap_ttf,    /*Function pointer to get glyph\'s bitmap*/\n\t'
                var_define += '.get_width = lv_font_get_width_ttf,  /*Function pointer to get glyph\'s width*/\n\t'
                var_define += '.bpp = 8,               /*Bit per pixel*/\n\t'
                var_define += '.monospace = 0,             /*Fix width (0: if not used)*/\n\t'
                var_define += '.next_page = NULL,      /*Pointer to a font extension*/\n\t'
                var_define += '.user_data = NULL,      /*Pointer to lv_font2_t, refer to lvgl 7.8.1*/\n'
                var_define += '};\n'
                c_content += var_define

            c_path = os.path.dirname(path) + '/' + pre_suffix + '.c'
            print(c_path)
            with open(c_path, 'w') as c_file:
                print(c_file)
                c_file.write(c_content)
            # print(c_content)
        else:
            bin_path = os.path.dirname(path) + '\\' + pre_suffix + '.bin'
            with open(bin_path, 'w') as bin_file:
                bin_file.write(r.text)
            # print(r.text)
    except Exception as err:
        self.s.close()
        print('post err = %s' % err)
        logging.exception(err)
        with open(self.log_file, 'a') as log_file:
            ltime = time.strftime("%Y-%m-%d %H:%M:%S: ", time.localtime())
            log_file.write(ltime + path + ': ' + str(err) + '\n')
        return False
    return True


if __name__ == "__main__":
    app = QApplication([])
    widget = ImageConvert()
    widget.show()
    '''
    lvgltoolsForm = ImageConvert()
    widget = QWidget()
    lvgltoolsForm.setupUi(widget)
    lvgltoolsForm.show()
    '''
    sys.exit(app.exec_())
