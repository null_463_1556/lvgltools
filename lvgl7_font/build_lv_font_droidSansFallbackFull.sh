#output C files
#python built_in_font_gen.py --compressed --size 20 -o lv_font_droidSansFallbackFull_20_compressed.c --bpp 2
python built_in_font_gen.py --size 20 -o lv_font_droidSansFallbackFull_20.c --bpp 2
python built_in_font_gen.py --compressed --size 30 -o lv_font_droidSansFallbackFull_30_compressed.c --bpp 2
#python built_in_font_gen.py --size 30 -o lv_font_droidSansFallbackFull_30.c --bpp 2

#ouput binary files
python built_in_font_gen.py --size 20 -o lv_font_droidSansFallbackFull_20.bin --bpp 2 --format bin
python built_in_font_gen.py --compressed --size 30 -o lv_font_droidSansFallbackFull_30_compressed.bin --bpp 2 --format bin
